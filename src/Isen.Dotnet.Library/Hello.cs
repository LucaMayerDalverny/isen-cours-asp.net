using System;

/// Déclaration du namespace, qui est un bloc 
namespace Isen.Dotnet.Library
{
    /// <summary>
    /// Cette classe vous dit bonjour
    /// </summary>
    public class Hello
    {
        /// Get / Set implicite
        public string Name { get; set; }

        public Hello(string name)
        {
            Name = name;
        }

        public string Greet() => $"Hello, {Name} !";
    }
}