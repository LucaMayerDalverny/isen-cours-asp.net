using System;
using System.Text;

namespace Isen.Dotnet.Library
{
    /// <summary>
    /// Classe pour la manipulation d'une liste de string
    /// </summary>
    public class MyCollection
    {
        private string[] _values;

        public int Count => _values.Length;

        public MyCollection()
        {
            _values = new string[0];
        }

        public MyCollection(string[] array)
        {
            _values = array;
        }

        public string this[int index]
        {
            get => _values[index];
            set => _values[index] = value;
        }

        public void Add(string item)
        {
            var tmpArray = new string[Count+1];
            for(var index = 0 ; index < Count ; index+=1)
            {
                tmpArray[index] = this[index];
            }
            tmpArray[Count] = item;
            _values = tmpArray;
        }

        public void RemoveAt(int index)
        {
            if(_values?.Length == null ||
               index > Count ||
               index < 0)
            {
                throw new IndexOutOfRangeException();
            }

            var tmpArray = new string[Count-1];

            for(var i = 0 ; i < tmpArray.Length ; i+=1)
            {
                tmpArray[i] = _values[i < index ? i : i+1];
            }
            
            _values = tmpArray;
        }

        public int IndexOf(string item)
        {
            var index = -1;

            for (int i = 0; i < Count; i++)
            {
                if(this[i].Equals(item))
                {
                    index = i;
                    break;
                }
            }

            return index;
        }



        public override string ToString()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.Append($"Dimension={Count} ");
            stringBuilder.Append("[ ");

            foreach(var value in _values)
            {
                stringBuilder.Append(value);
                stringBuilder.Append(" ");
            }

            stringBuilder.Append(" ]");

            return stringBuilder.ToString();
        }
    }
}