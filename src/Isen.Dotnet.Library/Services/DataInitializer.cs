using System;
using System.Collections.Generic;
using Isen.Dotnet.Library.Model;

namespace Isen.Dotnet.Library.Services
{
    public class DataInitializer
    {
        private List<string> _firstNames => new List<string>()
        {
            "Jean",
            "Bernard",
            "Michel",
            "Armand",
            "Roland",
            "Marcel"
        };
        private List<string> _lastNames => new List<string>(){
            "Dupuis",
            "Fernand",
            "Dupont",
            "Martin"
        };
        private List<string> _cities => new List<string>(){
            "Toulon",
            "Nice",
            "Marseille",
            "Aix"
        };

        private readonly Random _random;

        private string RandomFirstName => _firstNames[_random.Next(_firstNames.Count)];
        private string RandomLastName => _lastNames[_random.Next(_lastNames.Count)];
        private string RandomCity => _cities[_random.Next(_cities.Count)];
        private DateTime RandomDate => new DateTime(_random.Next(1980,2010),1,1).AddDays(_random.Next(0,365));

        private Person RandomPerson => new Person()
        {
            FirstName = RandomFirstName,
            LastName = RandomLastName,
            DateOfBirth = RandomDate,
            BirthCity = RandomCity,
            ResidenceCity = RandomCity
        };

        public List<Person> GetPersons(int size)
        {
            var persons = new List<Person>();
            for(var i = 0 ; i < size ; i+=1)
            {
                persons.Add(RandomPerson);
            }
            return persons;
        }

        public DataInitializer()
        {
            _random = new Random();
        }
    }
}