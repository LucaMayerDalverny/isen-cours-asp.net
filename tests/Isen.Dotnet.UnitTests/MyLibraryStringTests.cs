using System;
using Xunit;
using Isen.Dotnet.Library;

namespace Isen.Dotnet.UnitTests
{
    public class MyLibraryStringTests
    {
        private static string[] TestArray => new string[] {"Hello", "Brave", "New", "World"};
        private static MyCollection BuildTestList()
        {
            var myCollection = new MyCollection();
            foreach (var item in TestArray) myCollection.Add(item);
            return myCollection;
         
        }

        [Fact]
        public void CountTest()
        {
            var myCollection = BuildTestList();
            Assert.Equal(myCollection.Count,TestArray.Length);
        }

        [Fact]
        public void AddTest() => Assert.Equal(TestArray,BuildTestList().Values);

        [Fact]
        public void IndexOfTest()
        {
            var myCollection = BuildTestList();
            Assert.Equal(0,myCollection.IndexOf("Hello"));
            Assert.Equal(1,myCollection.IndexOf("Brave"));
            Assert.Equal(2,myCollection.IndexOf("New"));
            Assert.Equal(3,myCollection.IndexOf("World"));
            Assert.Equal(-1,myCollection.IndexOf("Toto"));
        }

        [Fact]
        public void RemoveAtTest()
        {
            var myCollection = BuildTestList();
            myCollection.RemoveAt(0);
            var targetArray = new string[] {"Brave", "New", "World"};
            Assert.Equal(targetArray,myCollection.Values);
        }

        [Fact]
        public void RemoveTest()
        {
            var myCollection = BuildTestList();

            Assert.False(myCollection.Remove("Toto"));
            Assert.True(myCollection.Remove("Hello"));
            Assert.False(myCollection.Remove("Hello"));
            Assert.True(myCollection.Remove("Brave"));
            Assert.True(myCollection.Remove("New"));
            Assert.True(myCollection.Remove("World"));
        }

        [Fact]
        public void EnumerableTest()
        {
            var myCollection = BuildTestList();
            var loops = 0;
            foreach (var item in myCollection)
            {
                loops+=1;
            }
            Assert.True(myCollection.Count == loops);
        }
    }
}
