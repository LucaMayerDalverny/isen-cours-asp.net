# ASP.NET

## Prérequis

* Installer VS Code
* Installer Dotnet Core SDK 3.0.100
* Installer un terminal serieux (Terminus, Cmder, ConEmu)
* Installer `git` et tester avec `git --version`
* Dossier de travail : `mkdir Isen.Dotnet && cd Isen.Dotnet`

## Préparation de la solution

## Création d'un dépôt git local

* `git init` pour créer un dépôt local
* Créer un `.gitignore` avec `touch .gitignore`

## Création d'un dépôt en ligne

* Créer un dépôt sur github, gitlab ou framagit
* Ajouter l'adresse du dépôt en ligne au dépôt local : `git remote add origin url`

## Création du projet console

* Dans le dossier de travail, créer un dossier `src/Isen.Dotnet.ConsoleApp`
* Utilisation de la dotnet cli pour générer le template de projet : `dotnet new console`

## Création du projet librairie

* Dans le dossier de travail, créer un dossier `src/Isen.Dotnet.Library`
* Utilisation de la dotnet cli pour générer le template de projet : `dotnet new classlib`

## Création du projet test

* Dans le dossier de travail, créer un dossier `src/Isen.Dotnet.Tests`
* Utilisation de la dotnet cli pour générer le template de projet : `dotnet new xunit`

## Architecture d'une solution

* `Isen.Dotnet` est lenom de la solution et en même temps la racine de tous les namespace
* `Isen.Dotnet.ConsoleApp` est un **Projet**. Son nom correspond au namespace du projet, lui même étant hiérarchiquement imbriqué au namespace de la solution.
* Le fichier projet a l'extension `.csproj`. C'est un fichier xml qui décrit :
    * Le type de projet
    * Ses dépendances
    * Le runtime utilisé
* Le workspace / solution dispose d'un fichier de solution, qui recence tous les projets de la solutioin, ainsi que les fichiers annexes. Ce fichier est de type `.sln` et va être créer ainsi : `dotnet new sln`